package com.gitlab.dyvol777.CellularAutomaton.Automatons

import doobie._
import doobie.implicits._

import scala.collection.mutable

abstract class Automaton(function: Long,
                         dimensionX: Int,
                         dimensionY: Int,
                         neighborhood: Boolean) {
  val funcNumVar: Int

  private val n = math.pow(2, dimensionX * dimensionY).toLong
  private val primary = getSmallestPrimary(n)
  private val state: mutable.BitSet = new mutable.BitSet(math.pow(2, dimensionX * dimensionY).toInt)

  @scala.annotation.tailrec
  final private def iterate(currentState: Int, startCycle: Int): Boolean = {
    //println(currentState)
    if (state(currentState))
      if (currentState == startCycle) {
        FirstNonZeroBit match {
          case -1 => true
          case x => iterate(x, x)
        }
      }
      else
        false
    else {
      state.add(currentState)
      iterate(nextStep(currentState), startCycle)
    }
  }

  private def FirstNonZeroBit: Int = {
    for (i <- 0 until n.toInt) {
      if (!state(i))
        return i
    }
    -1
  }

  lazy val Reversible: Boolean = iterate(0, 0)

  def saveToDb(): Update0 = {
    sql"insert into reversible_functions (function, dim_x, dim_y, func_num_var, neighborhood, linear, shift, reversible, anf) values (${"0" * (math.pow(2, funcNumVar).toInt - function.toBinaryString.length) + function.toBinaryString}, $dimensionX, $dimensionY, $funcNumVar, $neighborhood, $linear, $shift, $Reversible, $anf)".update
  }

  private def checkReversible: Boolean = {
    // (0 until n.toInt).par.map(nextStep).toSet.size == n
    // скорее всего дольше

    val state: mutable.BitSet = new mutable.BitSet(n.toInt)
    for (x <- 0 until n.toInt) {
      val y = nextStep(x)
      if (state(y))
        return false
      else
        state.add(y)
    }
    true
  }

  def nextStep(currentState: Int): Int = {
    val s = "0" * (dimensionX * dimensionY - currentState.toBinaryString.length) + currentState.toBinaryString
    val newState = s.indices.map(
      i => {
        val q: String = getNeighborhood(s, i)
        if (0 == (function & (1 << Integer.parseInt(q, 2)))) "0"
        else "1"
      }
    ).mkString("")

    assert(newState.length == dimensionX * dimensionY)
    Integer.parseInt(newState, 2)
  }

  lazy val ghegalkin: Long = {
    var g = ""
    var nf = ("0" * (math.pow(2, funcNumVar).toInt - function.toBinaryString.length) + function.toBinaryString).reverse
    while (nf.length > 1) {
      g = nf(0) + g
      nf = nf.zip(nf.tail).map(
        x => ((x._1.toString.toInt + x._2.toString.toInt) % 2).toString
      ).reduceLeft(_ + _)
    }
    g = nf + g
    BigInt(g, 2).toLong
  }

  lazy val anf: String =
    ghegalkin.toBinaryString.reverse.zipWithIndex.map {
      case ('1', 0) => "1"
      case ('1', n) => (for (i <- 0 until funcNumVar if (1 << i & n) != 0) yield s"x${funcNumVar - i}").mkString("*")
      case _ => ""
    }.filter(_ != "").reduceLeftOption(_ + " + " + _).getOrElse("zeros")

  lazy val linear: Boolean =
    (ghegalkin & (Range(0, funcNumVar).map(x => math.pow(2, math.pow(2, x))).foldLeft(0.0)(_ + _) + 1).toLong) == ghegalkin

  lazy val shift: Boolean =
    linear && (
      ((ghegalkin & 1) == 1 && ghegalkin.toBinaryString.count(_ == '1') == 2) ||
        ((ghegalkin & 1) == 0 && ghegalkin.toBinaryString.count(_ == '1') == 1)
      )


  private def checkCoef(arr: Array[Long]): Boolean = {
    //    for (x <- 0 until n.toInt) {
    //      println(x)
    //      println(nextStep(x))
    //      println(
    //        arr.zipWithIndex.
    //          map(
    //            a => Math.floorMod(
    //              a._1 * BigInt(x).modPow(a._2, primary).toInt,
    //              primary
    //            )
    //          ).reduce((x, y) => Math.floorMod(x + y, primary)) % primary
    //      )
    //      println(
    //        nextStep(x) ==
    //          arr.zipWithIndex.
    //            map(
    //              a => Math.floorMod(
    //                a._1 * BigInt(x).modPow(a._2, primary).toInt,
    //                primary
    //              )
    //            ).reduce((x, y) => Math.floorMod(x + y, primary)) % primary
    //      )
    //      println()
    //    }

    (0L until n).forall { x =>
      nextStep(x.toInt) == arr.zipWithIndex.
        map(
          a => Math.floorMod(
            a._1 * BigInt(x).modPow(a._2, primary).toInt,
            primary
          )
        ).reduce((x, y) => Math.floorMod(x + y, primary)) % primary
    }
  }

  lazy val PermutationPolynomial: String = {
    if (!Reversible)
      "None"
    else {
      try {
        val arr: Array[Long] = LagrangePolynomial
        assert(checkCoef(arr))
        arr.zipWithIndex.filter(_._1 != 0).map(x => x._1.toString + s"x^${x._2}").reduceLeft(_ + " + " + _)
      }
      catch {
        case _: Throwable => "Error"
      }
    }
  }

  private def getSmallestPrimary(i: Long): Long = {
    lazy val primes: LazyList[Long] = 2L #::
      LazyList.iterate(3L)(_ + 2L).filter(isPrime)

    def isPrime(n: Long): Boolean =
      primes.takeWhile(p => p * p <= n).forall(n % _ != 0)

    primes.find(_ > i).getOrElse(i)
  }

  private def getInverse(i: Long, p: Long): Long = {
    Array.tabulate(p.toInt)(x => x).map(_ * i % p).zipWithIndex.find(_._1 == 1).get._2
  }

  private lazy val LagrangePolynomial: Array[Long] = {
    if (!Reversible)
      Array(0L)
    else
      (0L until n).
        map { i =>
          val znam = Array.tabulate(n.toInt)(x => x).
            filter(_ != i).
            map(i - _).
            reduce((x, y) => Math.floorMod(x * y, primary))
          (0L until n).
            filter(_ != i).
            map(-_).
            foldLeft(List(1L)) { (x, y) =>
              (0L +: x).zipAll(
                x.map(k => Math.floorMod(k * y, primary)),
                0L,
                0L
              ).map(q => Math.floorMod(q._1 + q._2, primary))
            }.
            map(x => Math.floorMod(x * nextStep(i.toInt) * getInverse(znam, primary), primary))
        }.
        reduceLeft { (x, y) =>
          x.zipAll(y, 0L, 0L).map(z => Math.floorMod(z._1 + z._2, primary))
        }.
        toArray
  }


  def getNeighborhood(s: String, i: Int): String

  override def toString: String = {
    val t = System.nanoTime()
    s"Automate: dimX = $dimensionX, dimY = $dimensionY, neighborhood = $neighborhood, " +
      s"function = ${
        "0" * (math.pow(2, funcNumVar).toInt - function.toBinaryString.length) + function.toBinaryString
      }, " +
      s"linear = $linear, shift = $shift, " +
      s"reversible = $Reversible, \n" +
      s"anf = $anf \n" +
      //s"polynomial = $PermutationPolynomial \n" +
      s"evaluation time = ${
        (System.nanoTime() - t) / math.pow(10, 9)
      } \n \n"
  }

}
