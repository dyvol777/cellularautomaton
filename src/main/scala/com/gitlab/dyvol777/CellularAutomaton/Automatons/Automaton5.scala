package com.gitlab.dyvol777.CellularAutomaton.Automatons

import java.io.{BufferedWriter, File, FileWriter}
import java.util.Calendar

import scala.collection.mutable
import scala.collection.parallel.CollectionConverters._

class Automaton5 private(function: Long, dimensionX: Int, neighborhood: Boolean)
  extends Automaton(function, dimensionX, 1, neighborhood) {


  override val funcNumVar: Int = 5

  override def getNeighborhood(s: String, i: Int): String =
    if (neighborhood) {
      s"${s((i - 2 + dimensionX) % dimensionX)}" +
        s"${s((i - 1 + dimensionX) % dimensionX)}" +
        s"${s(i)}" +
        s"${s((i + 1 + dimensionX) % dimensionX)}" +
        s"${s((i + 2 + dimensionX) % dimensionX)}"
    } else {
      s"${if (i - 2 >= 0) s(i - 2) else 0}" +
        s"${if (i - 1 >= 0) s(i - 1) else 0}" +
        s"${s(i)}" +
        s"${if (i + 1 < dimensionX) s(i + 1) else 0}" +
        s"${if (i + 2 < dimensionX) s(i + 2) else 0}"

    }
}

object Automaton5 {
  def apply(function: Long, dimensionX: Int, neighborhood: Boolean): Automaton =
    new Automaton5(function, dimensionX, neighborhood)

  def test(maxdim: Int): Unit = {
    val file = new File("results/Automaton5.txt")
    file.createNewFile()
    val bw = new BufferedWriter(new FileWriter(file))


    bw.write("Function of 5 vals\n")
    bw.write(s"Start on ${Calendar.getInstance().getTime}\n")
    val t3 = System.nanoTime()
    Range(8, maxdim).foreach { i =>
      bw.write(s"start of dimension $i\n")

      //java.lang.OutOfMemoryError: Java heap space
      //More than Int.MaxValue elements. 4_294_967_296L > 2_147_483_647
      for (k <- 0L until 65536L) {
        //println(k)
        (65536L * k until 65536L * (k + 1)).par.foreach { x =>
          if (mutable.BitSet.fromBitMask(Array(x)).size == 16) {
            val q = Automaton5(x, i, neighborhood = true)
            if (q.Reversible)
              bw.write(s"Reversable $q\n")
          }
        }
      }

      for (k <- 0L until 65536L)
        (65536L * k until 65536L * (k + 1)).par.foreach { x =>
          if (mutable.BitSet.fromBitMask(Array(x)).size == 16) {
            val q = Automaton5(x, i, neighborhood = false)
            if (q.Reversible)
              bw.write(s"Reversable $q\n")
          }
        }

      bw.write(s"end of dimension $i\n")
    }
    bw.write(s"Calc on ${(System.nanoTime() - t3) / math.pow(10, 9)}sec\n")
    bw.close()
  }
}
