package com.gitlab.dyvol777.CellularAutomaton.Automatons

import scala.collection.mutable
import scala.collection.parallel.CollectionConverters._

class Automaton2Dwith5valsFunction private(function: Long, dimensionX: Int, dimensionY: Int, neighborhood: Boolean)
  extends Automaton(function, dimensionX, dimensionY, neighborhood) {
  override val funcNumVar: Int = 5

  override def getNeighborhood(s: String, i: Int): String = {
    val xi = i % dimensionX
    val yi = i / dimensionX
    if (neighborhood)
      s"${s((yi + 1 + dimensionY) % dimensionY * dimensionX + xi)}" +
        s"${s(yi * dimensionX + (xi + 1 + dimensionX) % dimensionX)}" +
        s"${s(i)}" +
        s"${s((yi - 1 + dimensionY) % dimensionY * dimensionX + xi)}" +
        s"${s(yi * dimensionX + (xi - 1 + dimensionX) % dimensionX)}"
    else
      s"${if (yi + 1 < dimensionY) s((yi + 1) * dimensionX + xi) else 0}" +
        s"${if (xi + 1 < dimensionX) s(yi * dimensionX + xi + 1) else 0}" +
        s"${s(i)}" +
        s"${if (yi - 1 >= 0) s((yi - 1) * dimensionX + xi) else 0}" +
        s"${if (xi - 1 >= 0) s(yi * dimensionX + xi - 1) else 0}"
  }
}


object Automaton2Dwith5valsFunction {
  def apply(function: Long, dimensionX: Int, dimensionY: Int, neighborhood: Boolean): Automaton =
    new Automaton2Dwith5valsFunction(function, dimensionX, dimensionY, neighborhood)

  def test(maxdim: Int): Unit = {
    import cats.effect._
    import doobie._
    import doobie.util.ExecutionContexts
    implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContexts.synchronous)

    val xa = Transactor.fromDriverManager[IO](
      "org.postgresql.Driver", // driver classname
      "jdbc:postgresql:postgres", // connect URL (driver-specific)
      "postgres", // user
      "postgres", // password
      Blocker.liftExecutionContext(ExecutionContexts.synchronous) // just for testing
    )

    val y = xa.yolo
    import y._

    val t3 = System.nanoTime()
    Range(3, 4).foreach { x =>
      Range(x, 7).foreach { y =>
        if (x * y <= maxdim) {
          //java.lang.OutOfMemoryError: Java heap space
          //More than Int.MaxValue elements. 4_294_967_296L > 2_147_483_647
          for (k <- 0L until 65536L) {
            (65536L * k until 65536L * (k + 1)).par.foreach { f =>
              if (mutable.BitSet.fromBitMask(Array(f)).size == 16) {
                val q = Automaton2Dwith5valsFunction(f, x, y, neighborhood = true)
                if (q.Reversible) {
                  q.saveToDb().quick.unsafeRunAsyncAndForget()
                  println(q)
                }
              }
            }
          }

          for (k <- 0L until 65536L) {
            (65536L * k until 65536L * (k + 1)).par.foreach { f =>
              if (mutable.BitSet.fromBitMask(Array(f)).size == 16) {
                val q = Automaton2Dwith5valsFunction(f, x, y, neighborhood = false)
                if (q.Reversible) {
                  q.saveToDb().quick.unsafeRunAsyncAndForget()
                  println(q)
                }
              }
            }
          }
        }
      }
    }

  }
}
