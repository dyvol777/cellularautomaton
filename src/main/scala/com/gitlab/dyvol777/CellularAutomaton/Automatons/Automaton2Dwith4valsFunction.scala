package com.gitlab.dyvol777.CellularAutomaton.Automatons

import scala.collection.parallel.CollectionConverters._

class Automaton2Dwith4valsFunction private(function: Long, dimensionX: Int, dimensionY: Int, neighborhood: Boolean)
  extends Automaton(function, dimensionX, dimensionY, neighborhood) {
  override val funcNumVar: Int = 4

  override def getNeighborhood(s: String, i: Int): String = {
    val xi = i % dimensionX
    val yi = i / dimensionX
    if (neighborhood)
      s"${s((yi + 1 + dimensionY) % dimensionY * dimensionX + xi)}" + //сверху
        s"${s(yi * dimensionX + (xi + 1 + dimensionX) % dimensionX)}" + //слева
        s"${s((yi - 1 + dimensionY) % dimensionY * dimensionX + xi)}" + //снизу
        s"${s(yi * dimensionX + (xi - 1 + dimensionX) % dimensionX)}" //справа
    else
      s"${if (yi + 1 < dimensionY) s((yi + 1) * dimensionX + xi) else 0}" +
        s"${if (xi + 1 < dimensionX) s(yi * dimensionX + xi + 1) else 0}" +
        s"${if (yi - 1 >= 0) s((yi - 1) * dimensionX + xi) else 0}" +
        s"${if (xi - 1 >= 0) s(yi * dimensionX + xi - 1) else 0}"
  }

}

object Automaton2Dwith4valsFunction {
  def apply(function: Int, dimensionX: Int, dimensionY: Int, neighborhood: Boolean): Automaton =
    new Automaton2Dwith4valsFunction(function, dimensionX, dimensionY, neighborhood)

  def test(maxdim: Int): Unit = {
    import cats.effect._
    import doobie._
    import doobie.util.ExecutionContexts
    implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContexts.synchronous)

    val xa = Transactor.fromDriverManager[IO](
      "org.postgresql.Driver", // driver classname
      "jdbc:postgresql:postgres", // connect URL (driver-specific)
      "postgres", // user
      "postgres", // password
      Blocker.liftExecutionContext(ExecutionContexts.synchronous) // just for testing
    )

    val y = xa.yolo
    import y._

    Range(4, 5).foreach { x =>
      Range(3, 6).foreach { y =>
        if (x * y <= maxdim) {

          Range(0, 65536).par.foreach { f =>
            val q = Automaton2Dwith4valsFunction(f, x, y, neighborhood = true)
            if (q.Reversible) {
              q.saveToDb().quick.unsafeRunSync
              println(q)
            }
          }
          Range(0, 65536).par.foreach { f =>
            val q = Automaton2Dwith4valsFunction(f, x, y, neighborhood = false)
            if (q.Reversible) {
              q.saveToDb().quick.unsafeRunSync
              println(q)
            }
          }
        }
      }
    }
  }
}