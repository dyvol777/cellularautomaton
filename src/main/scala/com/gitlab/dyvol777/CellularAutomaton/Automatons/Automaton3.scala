package com.gitlab.dyvol777.CellularAutomaton.Automatons

import java.io._

import scala.collection.parallel.CollectionConverters._

class Automaton3 private(function: Long, dimensionX: Int, neighborhood: Boolean)
  extends Automaton(function, dimensionX, 1, neighborhood) {


  override val funcNumVar: Int = 3

  override def getNeighborhood(s: String, i: Int): String =
    if (neighborhood) {
      s"${s((i - 1 + dimensionX) % dimensionX)}" +
        s"${s(i)}" +
        s"${s((i + 1 + dimensionX) % dimensionX)}"
    } else {
      s"${if (i - 1 >= 0) s(i - 1) else 0}" +
        s"${s(i)}" +
        s"${if (i + 1 < dimensionX) s(i + 1) else 0}"
    }
}

object Automaton3 {
  def apply(function: Int, dimensionX: Int, neighborhood: Boolean): Automaton =
    new Automaton3(function, dimensionX, neighborhood)

  def test(maxdim: Int): Unit = {
    val file = new File("results/Automaton3.txt")
    file.createNewFile()
    val bw = new BufferedWriter(new FileWriter(file))

    bw.write("Function of 3 vals\n")
    val t1 = System.nanoTime()
    Range(8, maxdim).foreach { i =>
      bw.write(s"start of dimension $i\n")
      Range(0, 256).par.foreach { x =>
        val q = Automaton3(x, i, neighborhood = true)
        if (q.Reversible)
          bw.write(s"Reversable $q\n")
      }
      Range(0, 256).par.foreach { x =>
        val q = Automaton3(x, i, neighborhood = false)
        if (q.Reversible)
          bw.write(s"Reversable $q\n")
      }
      bw.write(s"end of dimension $i\n")
    }
    bw.write(s"Calc on ${(System.nanoTime() - t1) / math.pow(10, 9)}sec\n")
    bw.close()
  }

}
