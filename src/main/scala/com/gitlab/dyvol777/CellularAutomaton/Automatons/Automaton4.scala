package com.gitlab.dyvol777.CellularAutomaton.Automatons

import java.io.{BufferedWriter, File, FileWriter}

import scala.collection.parallel.CollectionConverters._

class Automaton4 private(function: Long, dimensionX: Int, neighborhood: Boolean)
  extends Automaton(function, dimensionX, 1, neighborhood) {

  override val funcNumVar: Int = 4

  override def getNeighborhood(s: String, i: Int): String =
    if (neighborhood) {
      s"${s((i - 2 + dimensionX) % dimensionX)}" +
        s"${s((i - 1 + dimensionX) % dimensionX)}" +
        s"${s((i + 1 + dimensionX) % dimensionX)}" +
        s"${s((i + 2 + dimensionX) % dimensionX)}"
    } else {
      s"${if (i - 2 >= 0) s(i - 2) else 0}" +
        s"${if (i - 1 >= 0) s(i - 1) else 0}" +
        s"${if (i + 1 < dimensionX) s(i + 1) else 0}" +
        s"${if (i + 2 < dimensionX) s(i + 2) else 0}"

    }
}

object Automaton4 {
  def apply(function: Int, dimensionX: Int, neighborhood: Boolean): Automaton =
    new Automaton4(function, dimensionX, neighborhood)

  def test(maxdim: Int): Unit = {
    val file = new File("results/Automaton4.txt")
    file.createNewFile()
    val bw = new BufferedWriter(new FileWriter(file))

    bw.write("Function of 4 vals\n")
    val t2 = System.nanoTime()
    Range(8, maxdim).foreach { i =>
      bw.write(s"start of dimension $i\n")
      Range(0, 65536).par.foreach { x =>
        val q = Automaton4(x, i, neighborhood = true)
        if (q.Reversible)
          bw.write(s"Reversable $q\n")
      }
      Range(0, 65536).par.foreach { x =>
        val q = Automaton4(x, i, neighborhood = false)
        if (q.Reversible)
          bw.write(s"Reversable $q\n")
      }
      bw.write(s"end of dimension $i\n")
    }
    bw.write(s"Calc on ${(System.nanoTime() - t2) / math.pow(10, 9)}sec\n")
    bw.close()
  }
}
