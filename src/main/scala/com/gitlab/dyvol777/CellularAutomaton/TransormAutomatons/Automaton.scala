package com.gitlab.dyvol777.CellularAutomaton.TransormAutomatons

// function['0000'] = function(0)
// function['0010'] = function(2) (если dimension = 2)
abstract class Automaton(val function: String,
                         dimension: Int, // над какой размерностью функция
                         dimX: Int,
                         dimY: Int,
                         boundary: Boolean) // граница: true - периодическая
{
  def getNeighbours(i: Int, s: String, dimX: Int, dimY: Int): String

  private val funcNumVar: Int = (math.log(function.length) / math.log(dimension)).toInt // от скольки переменных фунция
  assert(math.pow(dimension, funcNumVar) == function.length)

  private val n = math.pow(dimension, dimX * dimY).toLong //количество внутренних состояний
  //  println("n = "+n)

  /*
    state - это текущее состояние.
    как это работает в двумерно случае:
    берем первые dimX элементов - первая строка
    и тд
  */
  def nextState(s: String, dx: Int, dy: Int): String = {
    val news = s.indices.map(
      i => {
        val q: String = getNeighbours(i, s, dx, dy)
        function(BigInt(q, dimension).toInt)
      }
    ).mkString("")


    assert(news.length == dx * dy)
    news
  }

  def printState(s: String): Unit = {
    //    println("dimY - " + dimY)
    for (i <- 0 until dimY)
      println(s.substring(i * dimX, (i + 1) * dimX))
  }

  lazy val reversible: Boolean = {
    (0L until n).
      map(BigInt(_).toString(dimension)).
      map(state => "0" * (dimX * dimY - state.length) + state).
      map(nextState(_, dimX, dimY)).
      toSet.
      size == n
  }

  def modify(s: String): String = {
    val q1 = s.substring(0, dimY)
    val q2 = s.substring(dimY, 2 * dimY)
    val q3 = s.substring(2 * dimY, 3 * dimY)

    var newS = ""
    for (i <- 0 until dimY)
      newS = newS + q1(i) + q2(i) + q3(i)
    newS
  }

  def transformToOneDimension: Automaton = {
    if (dimY == 1)
      this
    else {
      val k = math.pow(dimension, 3 * dimY).toInt
      val newD = math.pow(dimension, dimY).toInt

      // *** индексация
      val f = (0 until k).
        map(BigInt(_).toString(dimension)).
        map(state => "0" * (3 * dimY - state.length) + state).
        map(modify).
        map(nextState(_, 3, dimY)).
        map(_.zipWithIndex.filter(_._2 % 3 == 1).map(_._1).mkString("")).
        //        map(_.reverse).
        map(BigInt(_, dimension).toString(newD)).
        mkString("")


      new Automaton(("0" * (math.pow(newD, 3).toInt - f.length) + f), newD, dimX, 1, boundary) {
        override def getNeighbours(i: Int, s: String, dimX: Int, dimY: Int): String = if (boundary)
          s"${s((i - 1 + dimX) % dimX)}" +
            s"${s(i)}" +
            s"${s((i + 1 + dimX) % dimX)}"
        else
          s"${if (i - 1 >= 0) s(i - 1) else 0}" +
            s"${s(i)}" +
            s"${if (i + 1 < dimX) s(i + 1) else 0}"
      }
    }
  }

  override def toString: String = {
    val t = System.nanoTime()
    s"Automate: dimX = $dimX, dimY = $dimY, boundary = $boundary, " +
      s"function = ${function.reverse}, function lenght = ${function.length}, " +
      s"reversible = $reversible, " +
      s"evaluation time = ${(System.nanoTime() - t) / math.pow(10, 9)}"
  }
}
