package com.gitlab.dyvol777.CellularAutomaton

import com.gitlab.dyvol777.CellularAutomaton.Automatons._
import scala.collection.parallel.CollectionConverters._

import java.util.Calendar

object Main extends App {
  def test(): Unit = {
    //    import cats.effect._
    //    import doobie._
    //    import doobie.util.ExecutionContexts
    //
    //    // We need a ContextShift[IO] before we can construct a Transactor[IO]. The passed ExecutionContext
    //    // is where nonblocking operations will be executed. For testing here we're using a synchronous EC.
    //    implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContexts.synchronous)
    //
    //    // A transactor that gets connections from java.sql.DriverManager and executes blocking operations
    //    // on an our synchronous EC. See the chapter on connection handling for more info.
    //    val xa = Transactor.fromDriverManager[IO](
    //      "org.postgresql.Driver", // driver classname
    //      "jdbc:postgresql:postgres", // connect URL (driver-specific)
    //      "postgres", // user
    //      "postgres", // password
    //      Blocker.liftExecutionContext(ExecutionContexts.synchronous) // just for testing
    //    )
    //
    //    val y = xa.yolo
    //    import y._

    Range(0, 65536).par.foreach { x =>
      val a = Automaton2Dwith4valsFunction(x, 5, 3, neighborhood = true)
      if (a.Reversible) {
        print(a)
        //a.saveToDb().quick.unsafeRunSync
        //        println("0 -> " + a.nextStep(0))
        println()
      }
    }
  }

  val maxdim = 15
  // 1-dimension
  //  println("Start Automaton2")
  //  println(s"on ${Calendar.getInstance().getTime}")
  //  Automaton2.test(11)
  //  println("Start Automaton3")
  //  println(s"on ${Calendar.getInstance().getTime}")
  //  Automaton3.test(11)
  //  println("Start Automaton4")
  //  println(s"on ${Calendar.getInstance().getTime}")
  //  Automaton4.test(11)
  //  println("Start Automaton5")
  //  println(s"on ${Calendar.getInstance().getTime}")
  //  Automaton5.test(9)


  // 2-dimension
  //  println("Start Automaton2D4val")
  //  println(s"on ${Calendar.getInstance().getTime}")
  //  Automaton2Dwith4valsFunction.test(maxdim)
  //  println("Start Automaton2D5val")
  //  println(s"on ${Calendar.getInstance().getTime}")
  //  Automaton2Dwith5valsFunction.test(maxdim)

  // test
  test()
  println(s"Finished on ${Calendar.getInstance().getTime}")
}
