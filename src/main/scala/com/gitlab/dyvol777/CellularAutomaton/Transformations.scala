package com.gitlab.dyvol777.CellularAutomaton

import TransormAutomatons._

import scala.collection.parallel.CollectionConverters._

class A(function: String, d: Int, dimX: Int, dimY: Int, boundary: Boolean)
  extends Automaton(function: String, d: Int, dimX: Int, dimY: Int, boundary: Boolean) {
  override def getNeighbours(i: Int, s: String, dimX: Int, dimY: Int): String = {
    val xi = i % dimX
    val yi = i / dimX
    if (boundary)
      s"${s((yi + 1 + dimY) % dimY * dimX + xi)}" + //снизу
        s"${s(yi * dimX + (xi + 1 + dimX) % dimX)}" + // справа
        s"${s((yi - 1 + dimY) % dimY * dimX + xi)}" + // сверху
        s"${s(yi * dimX + (xi - 1 + dimX) % dimX)}" // слева
    else

        s"${if (xi + 1 < dimX) s(yi * dimX + xi + 1) else 0}" +
        s"${if (yi - 1 >= 0) s((yi - 1) * dimX + xi) else 0}" +
        s"${if (xi - 1 >= 0) s(yi * dimX + xi - 1) else 0}"+
    s"${if (yi + 1 < dimY) s((yi + 1) * dimX + xi) else 0}"
  }
}

object Transformations extends App {
//  Range(0, 65536).par.foreach { i =>
//    val f = BigInt(i).toString(2)

    val a = new A("1001001101101100".reverse, 2, 6, 3, false)
//    if(a.reversible) {
      println(a)
      //  a.printState("000000000000001")
      //  println("->")
      //  a.printState(a.nextState("000000000000001", 5, 3))


      val b = a.transformToOneDimension
      println(b)
//    }
//  }
  //  b.printState("7"*5*1)
  //  println("->")
  //  b.printState(b.nextState("7"*5*1, 5, 1))

  //  for(i<- (0 until 32768).map(BigInt(_).toString(8))){
  //    b.printState("0" * (5 - i.length) + i)
  //    println("->")
  //    b.printState(b.nextState("0" * (5 - i.length) + i, 5, 1))
  //    println()
  //  }


  //  assert(a.reversible == b.reversible)
  //  println()


  def test(): Unit = {
    val t = System.nanoTime()
    Range(0, 10).par.foreach { f =>
      val func = "0" * (32 - f.toBinaryString.length) + f.toBinaryString
      val a = new A(func.reverse, 2, 4, 3, true)
      val r = a.reversible
      //      println(a)
      //      val b = a.transformToOneDimension
      //      println(b)
      //      assert(a.reversible == b.reversible)
      //      println()

    }

    println((System.nanoTime() - t) / math.pow(10, 9) / 10)

    val t2 = System.nanoTime()
    Range(0, 10).par.foreach { f =>
      val func = "0" * (32 - f.toBinaryString.length) + f.toBinaryString
      val a = new A(func.reverse, 2, 4, 3, true)

      //      println(a)
      val b = a.transformToOneDimension
      //      println(b)
      //      assert(a.reversible == b.reversible)
      //      println()
      val r = a.reversible
    }

    println((System.nanoTime() - t2) / math.pow(10, 9) / 10)
  }

  //  test()

  //  val func = "0110110010010011"
  //  val a = new A(func.reverse, 2, 4, 4, false)
  //  val r = a.reversible
  //  println(a)
  //  val b = a.transformToOneDimension
  //  println(b)
  //  assert(a.reversible == b.reversible)
  //  println()
}